import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

open class SmartDevice(val name:String, val category: String) {
    
    var deviceStatus: String = "Online"
        protected set(value) {
            field = value
        }
    open val deviceType = "unknown"

    open fun turnOn() {
        //println("$name turned on")
        deviceStatus = "on"
    }

    open fun turnOff() {
        //println("$name turned off")
        deviceStatus = "off"
    }
}

class SmartTvDevice(deviceName: String, deviceCategory: String) : SmartDevice(name = deviceName, category = deviceCategory) {
    
    override val deviceType = "Smart TV"
    
    // private var speakerVolume = 2
    //     set(value) {
    //         if(value in 0..100) {
    //             field = value
    //         }
    //     }

    private var speakerVolume by RangeRegulator(intialValue = 2, minValue = 0, maxValue = 100)

    // private var channelNumber = 1
    //     set(value) {
    //         if(value in 1..200) {
    //             field = value
    //         }
    //     }
    private var channelNumber by RangeRegulator(intialValue = 1, minValue = 1, maxValue = 100)

    fun increaseVolume() {
        speakerVolume++
        println("Speaker volume is increased to $speakerVolume")
    }

    fun nextChannel() {
        channelNumber++
        println("Channel Number is increased to $channelNumber")
    }

    override fun turnOn() {
        //deviceStatus = "on"
        super.turnOn()
        println(
            "$name is turned on. Speaker volume is set to $speakerVolume and channel number is " +
                "set to $channelNumber."
        )
    }

    override fun turnOff() {
        //deviceStatus = "off"
        super.turnOff()
        println("$name turned off")
    }
}

class SmartLightDevice(deviceName: String, deviceCategory: String) : SmartDevice(name = deviceName, category = deviceCategory) {

    override val deviceType = "Smart Light"

    // private var brightnessLevel = 0
    //     set(value) {
    //         if(value in 0..100) {
    //             field = value
    //         }
    //     }

    private var brightnessLevel by RangeRegulator(intialValue = 0, minValue = 0, maxValue = 100)

    fun increaseBrightness() {
        brightnessLevel++
        println("Brightness increased to $brightnessLevel")
    }

    override fun turnOn() {
        //deviceStatus = "on"
        super.turnOn()
        brightnessLevel = 2
        println("$name turned on. The brightness level set to $brightnessLevel")
    }

    override fun turnOff() {
        //deviceStatus = "off"
        super.turnOff()
        brightnessLevel = 0
        println("Smart Light turned off")
    }
}

class SmartHome(val smartTvDevice: SmartTvDevice, val smartLightDevice: SmartLightDevice) {

    var deviceTurnOnCount: Int = 0
        private set(value) {
            field = value
        }

    fun turnOnTv() {
        deviceTurnOnCount++
        smartTvDevice.turnOn()
    }

    fun turnOffTv() {
        deviceTurnOnCount--
        smartTvDevice.turnOff()
    }

    fun increaseTvVolume() {
        smartTvDevice.increaseVolume()
    }

    fun changeTvChannelToNext() {
        smartTvDevice.nextChannel()
    }

    fun turnOnLight() {
        deviceTurnOnCount++
        smartLightDevice.turnOn()
    }

    fun turnOffLight() {
        deviceTurnOnCount--
        smartLightDevice.turnOff()
    }

    fun increaseLightBrightness() {
        smartLightDevice.increaseBrightness()
    }

    fun turnOffAllDevices() {
        turnOffTv()
        turnOffLight()
    }
}

class RangeRegulator(
    intialValue: Int,
    private val minValue: Int,
    private val maxValue: Int
) : ReadWriteProperty<Any?, Int> {

    var fieldData = intialValue

    override fun getValue(thisRef: Any?, property: KProperty<*>): Int {
        return fieldData
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        if(value in minValue..maxValue) {
            fieldData = value
        }
    }
}

fun main() {
    var smartDevice: SmartDevice = SmartTvDevice("Android TV", "Entertainment")
    smartDevice.turnOn()

    smartDevice = SmartLightDevice("Google Light", "Utility")
    smartDevice.turnOn()
}